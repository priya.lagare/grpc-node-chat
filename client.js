const grpc = require("grpc");
const protoLoader = require("@grpc/proto-loader");
const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const PROTO_PATH = "chat.proto";
const REMOTE_SERVER = "0.0.0.0:9090";

const packageDefinition = protoLoader.loadSync(PROTO_PATH);
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);

let username;

//Create gRPC client
const client = new protoDescriptor.ChatService(REMOTE_SERVER, grpc.credentials.createInsecure());

const startChat = () => {
    const channel = client.join({user: username});

    channel.on("data", onData);
    rl.on("line", (text) => {
        client.sendMsg({user: username, text: text}, res => {});
    })
}

const onData = (message) => {
    if(message.user === username) {
        return;
    }
    console.log(`${message.user}: ${message.text}`);
}

rl.question("Please type your name to join the chatgroup.. ", answer => {
    username = answer;
    
    startChat();
});
