const grpc = require("grpc");
const protoLoader = require("@grpc/proto-loader");

const PROTO_PATH = "chat.proto";
const SERVER_URI = "127.0.0.1:9090";

const usersInChat = [];
const observers = [];

const packageDefinition = protoLoader.loadSync(PROTO_PATH);
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);

// const join = (call, callback) => {
//     const user = call.request;

//     //check username already exists
//     const userExist = usersInChat.find(_user => _user.name == user.name);
//     if(!userExist) {
//         usersInChat.push(user);
//         callback(null, {
//             error: 0,
//             msg: "Success",
//         });
//     } else {
//         callback(null, {error: 1, msg: "user already exist."});
//     }
// }

// const sendMsg = (call, callback) => {
//     const chatObj = call.request;

//     observers.forEach(observer => {
//         observer.call.write(chatObj);
//     });
//     callback(null, {});
// }

let users = [];
// Receive message from client joining
function join(call, callback) {
    users.push(call);
    notifyChat({ user: "Server", text: "new user joined ..." });
  }
   
  // Receive message from client
  function sendMsg(call, callback) {
    notifyChat(call.request);
  }
   
  // Send message to all connected clients
  function notifyChat(message) {
    users.forEach(user => {
      user.write(message);
    });
  }

const getAllUsers = (call, callback) => {
    callback(null, {users: usersInChat});
}

const receiveMsg = (call, callback) => {
    observers.push({call});
}

const server = new grpc.Server();

server.addService(protoDescriptor.ChatService.service, {
    join,
    sendMsg,
    getAllUsers,
    receiveMsg,
});

server.bind(SERVER_URI, grpc.ServerCredentials.createInsecure());
server.start();
console.log("Server is running!");